import arcpy, os


root_folder = r"D:\GOHUNT\USGS Rivers and Streams V2\V1_NHD_Hyrdology_AllStates"

for gdb in os.listdir(root_folder):
    workspaces = os.path.join(root_folder, gdb)
    print(workspaces)

    ws_list = []

    if os.path.exists(workspaces):
        ws_list.append(workspaces)
    print(ws_list)

    for workspace in ws_list:
        arcpy.env.workspace = workspace
        print("The current workspace is {}".format(workspace))
        featureClasses = arcpy.ListFeatureClasses()

        in_table = []

        for fc in featureClasses:
            in_table.append(fc)
            
        # create no_codes list object
        no_codes = ["30700", "31200","31800","33400","33601","33603","34300","34305","34306","36100","36200","36400","36700","36701","36900","37800","39800","40300","40307","40308","40309","41100","42000","42001","42002","42003","42800","42801","42802","42803","42804","42805","42806","42807","42808","42809","42810","42811","42812","42813","42814","42815","42816","42820","42821","42822","42823","42824","43100","43400","43601","43603","43604","43605","43606","43607","43608","43609","43610","43611","43612","43623","43624","43625","43626","44100","44101","44102","44500","45000","45500","46007","46100","47800","48300","48400","48500","48700","48800","49300","50300","50301","50302","55800","56600","56700","56800"]
        # The no_codes list is a list of string values, but the FCode field is a long integer field, so turn the no_codes list object into an integer list object by changing the datatype to int for every i in no_codes list
        no_codes_int = [int(i) for i in no_codes]
 
        for fc in featureClasses:
            print(fc)
            in_table.append(fc)
            # Describe the .fields (reserved arcpy character) for the fc in the feature classes
            fields_obj = arcpy.Describe(fc).fields # only creates an object, not a list or anything
            # create list of the field names for fields in fields_obj object
            fields = [field.name for field in fields_obj]
            #check that FCode is in the list of fileds, if it does, create the update cursor.
            if 'FCode' in fields:
                no_codes_list_stringified = ','.join(map(str,no_codes_int))
                query = 'FCode in ({})'.format(no_codes_list_stringified)
                # arcpy.updateCursor(dataset, {where_clause}, {spatial_reference}, {fields}, {sort_fields})
                cursor = arcpy.da.UpdateCursor(fc, "FCode", query) # creates an update cursor of the feature class with features selected by the where clause
                for row in cursor: 
                    cursor.deleteRow()
                del cursor
                print("Deleted features for {}.".format(fc))
            else:
                print("Sweet!")
